const fs = require('fs');
const csv = require('csvtojson');

const MATCHES_FILE_PATH = '../data/matches.csv';
const DELIVERIES_FILE_PATH = '../data/deliveries.csv';

const tossWinsAndMatchWins = require('../ipl/tossWinsAndMatchWins.js');
const highestPlayerOfTheMatchAwards = require('../ipl/highestPlayerOfTheMatchAwards');
const strikeRate = require('../ipl/strikeRate.js')
const highestDismissalsByPlayer = require('../ipl/highestDismissalsByPlayer.js')
const bestBowlerInSuperOvers = require('../ipl/bestBowlerInSuperOvers.js')

const JSON_OUTPUT_FILE_PATH_1 = '../public/output/tossWinsAndMatchWins.json';
const JSON_OUTPUT_FILE_PATH_2 = '../public/output/highestPlayerOfTheMatchAwards.json';
const JSON_OUTPUT_FILE_PATH_3 = '../public/output/strikeRate.json'
const JSON_OUTPUT_FILE_PATH_4 = '../public/output/highestDismissalsByPlayer.json'
const JSON_OUTPUT_FILE_PATH_5 = '../public/output/bestBowlerInSuperOvers.json'




function main(){
  csv()
    .fromFile(MATCHES_FILE_PATH)
    .then((matches)=> {
      csv()
        .fromFile(DELIVERIES_FILE_PATH)
        .then( (deliveries => {

          let result1 = tossWinsAndMatchWins(matches);
          let result2 = highestPlayerOfTheMatchAwards(matches);
          let result3 = strikeRate(matches,deliveries);
          let result4 = highestDismissalsByPlayer(deliveries);

          let result5 = bestBowlerInSuperOvers(deliveries);

          saveResult1(result1);
          saveResult2(result2);
          saveResult3(result3);
          saveResult4(result4);
          saveResult5(result5);

        })

        )
    })

}

function saveResult1(result1){
  const jsonData = {
    tossWinsAndMatchWins : result1
  }
  
  const jsonString = JSON.stringify(jsonData);
  
  fs.writeFile(JSON_OUTPUT_FILE_PATH_1, jsonString, "utf-8", err =>{
    if (err) {
      console.log (err);
    }
  } )
}

function saveResult2(result2){
  const jsonData = {
    highestPlayerOfTheMatchAwards : result2
  }
  
  const jsonString = JSON.stringify(jsonData);
  
  fs.writeFile(JSON_OUTPUT_FILE_PATH_2, jsonString, "utf-8", err =>{
    if (err) {
      console.log (err);
    }
  } )
}

function saveResult3(result3){
  const jsonData = {
    strikeRate: result3
  }

  const jsonString = JSON.stringify(jsonData);

  fs.writeFile(JSON_OUTPUT_FILE_PATH_3,jsonString,"utf-8",err=>{
    if (err){
      console.log(err);
    }
  })
}

function saveResult4(result4){
  const jsonData = {
    highestDismissalsByPlayer : result4
  }
  
  const jsonString = JSON.stringify(jsonData);
  
  fs.writeFile(JSON_OUTPUT_FILE_PATH_4, jsonString, "utf-8", err =>{
    if (err) {
      console.log (err);
    }
  } )
}


function saveResult5(result5){
  const jsonData = {
    bestBowlerInSuperOvers: result5
  }

  const jsonString = JSON.stringify(jsonData);

  fs.writeFile(JSON_OUTPUT_FILE_PATH_5,jsonString,"utf-8",err=>{
    if (err){
      console.log(err);
    }
  })
}



main();


