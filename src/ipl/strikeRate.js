function strikeRate(matches,deliveries){

    let seasons = [];
    matches.map(match => {
      if (!seasons.includes(match['season'])){
        seasons.push(match['season']);
      }
    });
  
    
    const batsmanSelected = 'V Kohli';
    let strikeRate = {};
  
    
    seasons.map(season => {
      let idsForSeason = [];
      matches
            .filter(match => match['season'] ===season)
            .map(match => idsForSeason.push(match['id']));
      
      let totalRuns = [];
      deliveries
            .filter(delivery => idsForSeason.includes(delivery['match_id']) && delivery['batsman']===batsmanSelected)
            .map(delivery => {
              totalRuns.push(parseInt(delivery['total_runs']))
            })
      let totalBalls = totalRuns.length;

      if (totalRuns === []){
        strikeRate[season] = 0;
      } else {
        strikeRate[season] = (totalRuns.reduce((acc,val)=>acc+val,0)/totalBalls * 100).toFixed(2) ;
      }
      
    });
    
  
  return strikeRate;
  
  
  }

  module.exports = strikeRate