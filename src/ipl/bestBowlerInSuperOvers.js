function bestBowlerInSuperOvers(deliveries){
    let deliveriesConsidered = deliveries.filter(delivery => delivery['is_super_over']!=='0')
  
    let bowlers = [];
    deliveriesConsidered.map(delivery => {
      if (!bowlers.includes(delivery['bowler'])){
        bowlers.push(delivery['bowler']);
      }
    });
  
    
    
  
    let bowlersEconomy = {};
    
    bowlers.map(bowler => {
      let totalRuns = [];
      
      deliveriesConsidered
            .filter(delivery => delivery['bowler'] === bowler )
            .map(delivery => {
              totalRuns.push(parseInt(delivery['total_runs']));
            });
  
            let totalOvers = (totalRuns.length/6).toFixed(2);
  
            bowlersEconomy[bowler] = (totalRuns.reduce((acc,val)=>acc+val,0)/totalOvers).toFixed(2);
      });
  
      let bestBowler = Object.keys(bowlersEconomy).sort((a,b)=> bowlersEconomy[a] - bowlersEconomy[b]).slice(0,1);
  
      let result = {};
      result[bestBowler] = bowlersEconomy[bestBowler];
      return result;
      
  
      
    
  
  }

  module.exports = bestBowlerInSuperOvers

  