// Find the number of times each team won the toss and also won the match

function tossWinsAndMatchWins(matches){
  
    //find all teams
    // loop through when 1=2 wins++
    // at the end of iteration result[team] = wins
  
    let teams = [];
    matches.map(match => {
      teams.push(match['team1']);
      teams.push(match['team2']);
    });
    
    teams = Array.from(new Set(teams));
    
    let result = {};
    teams.map(team=> {
      let wins = matches.filter(match => match['team1']===team || match['team2']===team && match['winner']===team).length;
  
      result[team] = wins;
    });
  
    return result;
    
    
  }
  
module.exports = tossWinsAndMatchWins
