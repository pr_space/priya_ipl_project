function highestDismissalsByPlayer(deliveries){
    // { player dismissed : 
    //        {bowler : count}}
    let dismissals = {};
    deliveries
              .filter(delivery => delivery['dismissal_kind']!=='')
              .map(delivery => {
                const playerDismissed = delivery['player_dismissed'];
                const bowler = delivery['bowler']
                if (dismissals[playerDismissed]){
                  if (dismissals[playerDismissed][bowler]){
                    dismissals[playerDismissed][bowler] += 1;
                  } else {
                    dismissals[playerDismissed][bowler] = 1;
                  }
                
                } else {
                  dismissals[playerDismissed] = {};
                  dismissals[playerDismissed][bowler] = 1;
                }
    });
    
    
    
    let highestDismissals = {};

    for (let player in dismissals){
      const unsortedObj = dismissals[player];
      let playerWhoDismissed = Object.keys(unsortedObj).sort((a,b) => unsortedObj[b] - unsortedObj[a]).slice(0,1);

      highestDismissals[player] = {};
      highestDismissals[player][playerWhoDismissed] = dismissals[player][playerWhoDismissed];
      
      
    }

    return highestDismissals;
  }

  module.exports = highestDismissalsByPlayer