function highestPlayerOfTheMatchAwards(matches){
    // { season : {object of player : no of wins}}
  
    let seasons = [];
    matches.map(match => {
      if (!seasons.includes(match['season'])){
        seasons.push(match['season'])
      }
      });
    
    
  
    let playerAndAwardWins = {};
  
    seasons.map(season => {
      let awardsInASeason = {};
      matches.filter(match => match['season'] === season).map(match => {
        const awardee = match['player_of_match'];
  
        if (awardsInASeason[awardee]){
          awardsInASeason[awardee] +=1; 
        } else {
          awardsInASeason[awardee] = 1;
        }
      });

      
  
      let playerWithhighestWins = Object.keys(awardsInASeason).sort((a,b)=> awardsInASeason[b]- awardsInASeason[a]).slice(0,1);
  
      
  
      let result = {};
      result[playerWithhighestWins] = awardsInASeason[playerWithhighestWins];
  
  
      playerAndAwardWins[season] = result;
  
  
    });
  
    return (playerAndAwardWins);
  
  }
  

  module.exports = highestPlayerOfTheMatchAwards